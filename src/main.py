"""
CZ1003 mini project
main script to start canteen recommendation system
@author li zhaonian
"""

import pygame
import map, terminal
 


# entry point of the system
if __name__ == "__main__":
    
    #first print header
    terminal.print_header()
    #use map module to retrieve user position
    selectedPos = map.start_pygame()
    #switch to terminal to display various list of recommentdation 
    # terminal.menu()
    menu = True
    
    while menu:
        terminal.menu(selectedPos)
        option = input("Select your option: ")
        if option == '1':
            terminal.canteen_distance_rank(selectedPos)
        elif option == '2':
            terminal.canteen_rank()
        elif option == '3':
            terminal.search_food()
        elif option == '4':
            terminal.sort_price()
        elif option == '5':
            terminal.search_halal()
        elif option == '6':
            selectedPos = map.change_location(selectedPos)
        elif option == '-1':
            menu = False

    print("Thank you for using our system!")




  
