from operator import itemgetter
import math,canteen

canteens = canteen.get_canteen_info()

def searchFoodCanteen(food, canteensDic):
    canteenList = []
    for canteen in canteensDic:
        for key in canteensDic[canteen]["Foods"]:
            modfood = food.replace(" ", "").lower()
            if modfood in key:
                canteenList.append(canteen + ": " + key.title())
                break       
    return canteenList


# To calculate distance
def distCanteen(u0, d1):
    return math.sqrt((u0[0] - d1[0])**2 + (u0[1] - d1[1])**2)


# Function to sort distance based on user location to each canteen from nearest to farthest
def sort_distance(info):
    sort_info = sorted(info, key = itemgetter(2))        
    return sort_info


# To calculate the distance from user location to each canteen
def calDist(userPos):
    disList = []
    for i in canteens:
        for j in canteens[i]:
            totalDist = distCanteen(userPos,canteens[i]['Position'])
            allDist = [i,canteens[i]['Location'],round(totalDist,2)]
            disList.append(allDist)
            break
    return disList