"""
CZ1003 mini project
this is to implement terminal menu after user select position
@author li zhaonian
"""

import os,canteen,function


# call get canteen info function to prepare canteen info data
canteens = canteen.get_canteen_info()

#function to print a nice header in terminal
def print_header():
    #first clear terminal
    os.system('clear')

    print("""
                        **********************************
                        **                              **
                        **    NTU F&B Recommendation    **
                        **                              **
                        **********************************

=======================================================================================

Welcome!
    """)


def menu(curPos):
    print(
f"""

 YOUR CURRENT POSITION: {curPos}
|---------------------------------|
|  1. Resturants List(Distance)   |
|  2. Resturants Rank             |
|  3. Search Food                 |
|  4. Sort By Food Price          |
|  5. Halal Resturants            |
|  6. Re-select Current Position  |
| -1. Terminate System            |
|---------------------------------|
"""
)

def canteen_menu():
    print("""
Canteen List:  
|----------------------------------|
|  1. Food Court 1                 |
|  2. Food Court 2                 |
|  3. Food Court 9                 |
|  4. Food Court 11                |
|  5. Food Court 13                |
|  6. Food Court 16                |
|  7. Ananda Kitchen               |
|  8. Foodgle Food Court           |
|  9. North Hill Food Court        |
| 10. Pioneer Food Court           |
| 11. Quad Cafe                    |
| 12. North Spine Food Court       |
| 13. The Sandwich Guys            |
| 14. Koufu @ the South Spine      |
| 15. NIE Canteen                  |
| -1. Return to previous menu      |
|----------------------------------|
"""
)


"""
below functions will make use of the alogrithm module to compute the correct list for display
"""
def canteen_distance_rank(userPos):
    print(100 * "=")
    print("This is a canteen list sorted by distance(ascending order) to your location\nCanteen name,"," Address", ", Distance")
    print(100 * "=")
    # To get the calculated distance from user location to each canteen
    dist = function.calDist(userPos)
    # To sort canteen based nearest distance from user location
    sortedInfo = function.sort_distance(dist)
    # To print Canteen name and Address location
    for i in range(len(sortedInfo)):
        # index = i
        # data_str = str(sortedInfo[index]).replace("'",'').strip('[]')
        print(f"{sortedInfo[i][0]} | {sortedInfo[i][1]} | {round(sortedInfo[i][2]*3.2*0.001,2)}km")
    print(100 * "=")


def canteen_rank():
    print("This is a canteen list sorted by rank.")

    # convert to list
    # [('canteen 1', {'Rank': 1}), ('canteen 2', {'Rank': 2}), ('canteen 3', {'Rank': 3})]
    items = []
    for name, item in canteens.items():
        items.append((name, item))

    # sort by Rank
    items.sort(key=lambda x: x[1]["Rank"])


    items = list(map(lambda x: ["Rank: " + str(x[1]["Rank"]), x[0]] , items))
    rank_list = [item for sublist in items for item in sublist]
    
    rank_list= "\n".join(rank_list)
    print(rank_list)
    print(100 * "=")


def search_food():
    search = True
    food = ""
    while search == True:
        food = input("Input food name: ")
        remspaceFood = food.replace(" ", "")
        if remspaceFood.isalpha():
                search = False
        else: 
            print("Please enter food name in alphabet\n")
            continue
    
    canlist = function.searchFoodCanteen(food,canteens)
    if canlist == []:
        print(f"There is no canteen selling {food} in NTU.") 
    else:
        print("The below is the list of canteen matches your search:\n")
        print("\n".join(canlist))
    print(100 * "=")


def sort_price():
        
    while True:
        canteen_menu()
        option = input("\n  Select your option: ")
        if option == '1':
            foods = sorted(canteens['Food Court 1']["Foods"].items(), key=lambda x: x[1])
            print("------------Food Court 1------------")
            for food in foods:
                s_food= food[0].title() + ": $" + food[1]
                print (s_food)
        elif option == '2':
            foods = sorted(canteens['Food Court 2']["Foods"].items(), key=lambda x: x[1])
            print("------------Food Court 2------------")
            for food in foods:
                s_food= food[0].title() + ": $" + food[1]
                print (s_food)
        elif option == '3':
            foods = sorted(canteens['Food Court 9']["Foods"].items(), key=lambda x: x[1])
            print("------------Food Court 9------------")
            for food in foods:
                s_food= food[0].title() + ": $" + food[1]
                print (s_food)
        elif option == '4':
            foods = sorted(canteens['Food Court 11']["Foods"].items(), key=lambda x: x[1])
            print("------------Food Court 11------------")
            for food in foods:
                s_food= food[0].title() + ": $" + food[1]
                print (s_food)
        elif option == '5':
            foods = sorted(canteens['Food Court 13']["Foods"].items(), key=lambda x: x[1])
            print("------------Food Court 13------------")
            for food in foods:
                s_food= food[0].title() + ": $" + food[1]
                print (s_food)
        elif option == '6':
            foods = sorted(canteens['Food Court 16']["Foods"].items(), key=lambda x: x[1])
            print("------------Food Court 16------------")
            for food in foods:
                s_food= food[0].title() + ": $" + food[1]
                print (s_food)
        elif option == '7':
            foods = sorted(canteens['Ananda Kitchen']["Foods"].items(), key=lambda x: x[1])
            print("------------Ananda Kitchen------------")
            for food in foods:
                s_food= food[0].title() + ": $" + food[1]
                print (s_food)
        elif option == '8':
            foods = sorted(canteens['Foodgle Food Court']["Foods"].items(), key=lambda x: x[1])
            print("------------Foodgle Food Court------------")
            for food in foods:
                s_food= food[0].title() + ": $" + food[1]
                print (s_food)
        elif option == '9':
            foods = sorted(canteens['North Hill Food Court']["Foods"].items(), key=lambda x: x[1])
            print("------------North Hill Food Court------------")           
            for food in foods:
                s_food= food[0].title() + ": $" + food[1]
                print (s_food)
        elif option == '10':
            foods = sorted(canteens['Pioneer Food Court']["Foods"].items(), key=lambda x: x[1])
            print("------------Pioneer Food Court------------")
            for food in foods:
                s_food= food[0].title() + ": $" + food[1]
                print (s_food)
        elif option == '11':
            foods = sorted(canteens['Quad Cafe']["Foods"].items(), key=lambda x: x[1])
            print("------------Quad Cafe------------")
            for food in foods:
                s_food= food[0].title() + ": $" + food[1]
                print (s_food)
        elif option == '12':
            foods = sorted(canteens['North Spine Food Court']["Foods"].items(), key=lambda x: x[1])
            print("------------North Spine Food Court------------")
            for food in foods:
                s_food= food[0].title() + ": $" + food[1]
                print (s_food)
        elif option == '13':
            foods = sorted(canteens['The Sandwich Guys']["Foods"].items(), key=lambda x: x[1])
            print("------------The Sandwich Guys------------")
            for food in foods:
                s_food= food[0].title() + ": $" + food[1]
                print (s_food)
        elif option == '14':
            foods = sorted(canteens['Koufu @ the South Spine']["Foods"].items(), key=lambda x: x[1])
            print("------------Koufu @ the South Spine------------")
            for food in foods:
                s_food= food[0].title() + ": $" + food[1]
                print (s_food)
        elif option == '15':
            foods = sorted(canteens['NIE Canteen']["Foods"].items(), key=lambda x: x[1])
            print("------------NIE Canteen------------")
            for food in foods:
                s_food= food[0].title() + ": $" + food[1]
                print (s_food)
        elif option == '-1':
            break



def search_halal():
    print("Halal Food Court(s): ")
    for name,i in canteens.items():
        if canteens[name]['Halal'] == True:
            print(name) 

    print(100 * "=")