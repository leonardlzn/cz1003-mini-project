"""
CZ1003 mini project
this script contain functions for displaying map and getting user position
@author li zhaonian
"""

import pygame,sys

#default system variable 
WIDTH = 1000
HEIGHT = 800
MAP_FILE = "../doc/ntu_map.png"
INTRO_FILE = "../doc/intro.png"
LOCATION_FILE = "../doc/gps.png"
TTF_FILE = "../open-sans/OpenSans-Bold.ttf"


# render text
def print_text(fontSize,content):
    text = pygame.font.Font(TTF_FILE,fontSize)
    textSurface = text.render(content,True,(0,0,0))
    textRect = textSurface.get_rect()
    return textSurface, textRect
      

# mark location on the map
def mark_location(screen,gpsFile,currentPos):
    # this is to make sure the marker is not initialized in the beginning
    if currentPos != [0,0]:
        gpsImg = pygame.image.load(gpsFile)
        screen.blit(gpsImg,(currentPos[0]-20,currentPos[1]-35))


# display the map
def display_map(width,height,mapFile,currentPos):
    pygame.display.set_caption('NTU F&B Recommendation')
    surfaceImg = pygame.image.load(mapFile)
    screen = pygame.display.set_mode((width,height))
    screen.blit(surfaceImg,(0,0))

    pygame.draw.rect(screen, (192, 206, 179),(700,0,300,300))
    infoSurface,infoRect = print_text(20,f"Selected Position: {currentPos}")
    infoRect.center = ((700+(300/2)), (300/2))
    screen.blit(infoSurface,infoRect)

    #mark the user position
    mark_location(screen,LOCATION_FILE,currentPos)

    #confirm button
    pygame.draw.rect(screen, (187, 192, 201),(800,200,120,60))
    mouse = pygame.mouse.get_pos()
    if 800+120 > mouse[0] > 800 and 200+60 > mouse[1] > 200:
        pygame.draw.rect(screen, (140, 255, 40),(800,200,120,60))
        # pygame.mouse.set_cursor(pygame.cursors.diamond)
    
        
    # text of the button
    btnSurface,btnRect = print_text(20,"Confirm")
    btnRect.center = ((800+(120/2)), (200+(60/2)))
    screen.blit(btnSurface,btnRect)

    pygame.display.update()


# fn to start intro window
def start_intro(introFile,sysName):
    pygame.display.set_caption('NTU F&B Recommendation')
    intro = True
    while intro:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()

        #set the intro background image and system name
        screen = pygame.display.set_mode((970,520))
        introImg = pygame.image.load(introFile)
        screen.blit(introImg,(0,0))
        textSurface,textRect = print_text(55,sysName)
        textRect.center = ((500,270))
        screen.blit(textSurface,textRect)        

        #initialize menu button and detect mouse click on button
        mouse = pygame.mouse.get_pos()
        if 400+100 > mouse[0] > 400 and 350+50 > mouse[1] > 350:
            pygame.draw.rect(screen, (140, 255, 40),(400,350,100,50))
            # pygame.mouse.set_cursor(pygame.cursors.diamond)
            if event.type == pygame.MOUSEBUTTONDOWN:
                intro=False
        else:
            pygame.draw.rect(screen, (187, 192, 201),(400,350,100,50))
        
        # text of the button
        btnSurface,btnRect = print_text(30,"Start")
        btnRect.center = ((400+(100/2)), (350+(50/2)))
        screen.blit(btnSurface,btnRect)
       
        pygame.display.update()



# main fn to initialize pygame window
# it will return the user selected position
def start_pygame():
    pygame.init()
    #start with intro
    start_intro(INTRO_FILE,"NTU F&B Recommendation")

    selectedPos = [0,0]
    end = False #end game flag
    while end != True:
        display_map(WIDTH,HEIGHT,MAP_FILE,selectedPos)
        #get list of events 
        events = pygame.event.get()
        for event in events:
            #check if event type is mouse click to get current position 
            if event.type == pygame.MOUSEBUTTONDOWN:
                currentPos = pygame.mouse.get_pos()
                # screen = pygame.display.set_mode((WIDTH,HEIGHT))
                if 800+120 > currentPos[0] > 800 and 200+60 > currentPos[1] > 200:
                    #scale down the window size for user to continue on terminal
                    pygame.display.set_mode((100,100))
                    pygame.display.flip()
                    end = True  

                elif currentPos[0]>700:
                    None

                else:
                    selectedPos[0] = currentPos[0]
                    selectedPos[1] = currentPos[1]
            
            #end game
            elif event.type == pygame.QUIT:
                sys.exit()

    
    return selectedPos
        

#similar to the map initialization function, but it serves the purpose of changing location
def change_location(curLocation):
    pygame.init()

    selectedPos = curLocation
    end = False #end game flag
    while end != True:
        display_map(WIDTH,HEIGHT,MAP_FILE,selectedPos)
        #get list of events 
        events = pygame.event.get()
        for event in events:
            #check if event type is mouse click to get current position 
            if event.type == pygame.MOUSEBUTTONDOWN:
                currentPos = pygame.mouse.get_pos()
                # screen = pygame.display.set_mode((WIDTH,HEIGHT))
                if 800+120 > currentPos[0] > 800 and 200+60 > currentPos[1] > 200:
                    pygame.display.set_mode((100,100))
                    pygame.display.flip()
                    end = True

                elif currentPos[0]>700:
                    None

                else:
                    selectedPos[0] = currentPos[0]
                    selectedPos[1] = currentPos[1]
            
            #end game
            elif event.type == pygame.QUIT:
                sys.exit()

    return selectedPos