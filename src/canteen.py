"""
CZ1003 mini project
module for canteen information extraction from excel file
@author li zhaonian
"""

import openpyxl

CANTEEN_FILE_PATH = "../doc/Canteen Info.xlsx"

# get the work book we are working on 
CANTEEN_WB = openpyxl.load_workbook(CANTEEN_FILE_PATH)

# get the particular sheet
CANTEEN_ACTIVE_SHEET = CANTEEN_WB['canteen info']

# get the max row of the sheet
MAX_ROW = CANTEEN_ACTIVE_SHEET.max_row

"""
A - canteen
B - signature
C - rank
D - service_hour
E - halal
F - position
G - location
H - food1
I - food2
J - food3
K - food4
L - food5
"""

# canteens = {
#      "Food Court 1":{
#          "Signature Food":"Japanese Fried Chicken Curry Rice",
#          "Rank":3,
#          "Service Hours":(7,21),
#          "Halal":True,
#          "Position":(430,200),
#          "Foods":{"japanese fried chicken curry rice":"6.00",
#                   "fishball noodles":"3.00",
#                   "prata":"1.20",
#                   "rosti with bratwurst":"5.50",
#                   "white bee hoon":"7.00"}
#      },
#      "North Hill Food Court":{
#          "Signature Food":"Mala Hotpot",
#          "Rank":2,
#          "Service Hours":(7,21),
#          "Halal":True,
#          "Position":(229,246),
#          "Foods":{"mala hotpot":"10.00",
#                   "fishball noodles":"2.50",
#                   "carrot cake":"2.50",
#                   "japanese bento":"5.50",
#                   "fried rice":"3.50"}
#      }
#  }


# return dict of canteen info
def get_canteen_info():
    returnObj = {}
    for row in range(2,MAX_ROW+1):
        canteen = str(CANTEEN_ACTIVE_SHEET['A' + str(row)].value)
        returnObj[canteen] = {}
        returnObj[canteen]["Signature"] =  CANTEEN_ACTIVE_SHEET['B' + str(row)].value
        returnObj[canteen]["Rank"] = CANTEEN_ACTIVE_SHEET['C' + str(row)].value
        # split service by "," and construct a service tuple
        returnObj[canteen]["Service_hour"] = (int(CANTEEN_ACTIVE_SHEET['D' + str(row)].value.split(",")[0]),int(CANTEEN_ACTIVE_SHEET['D' + str(row)].value.split(",")[1]))
        returnObj[canteen]["Halal"] = CANTEEN_ACTIVE_SHEET['E' + str(row)].value
        # split position by " " and construct a position tuple
        returnObj[canteen]["Position"] = (int(CANTEEN_ACTIVE_SHEET['F' + str(row)].value.split(" ")[0]),int(CANTEEN_ACTIVE_SHEET['F' + str(row)].value.split(" ")[1]))
        returnObj[canteen]["Location"] = CANTEEN_ACTIVE_SHEET['G' + str(row)].value
        returnObj[canteen]["Foods"] = {
            CANTEEN_ACTIVE_SHEET['H' + str(row)].value.split(":")[0]:CANTEEN_ACTIVE_SHEET['H' + str(row)].value.split(":")[1],
            CANTEEN_ACTIVE_SHEET['I' + str(row)].value.split(":")[0]:CANTEEN_ACTIVE_SHEET['I' + str(row)].value.split(":")[1],
            CANTEEN_ACTIVE_SHEET['J' + str(row)].value.split(":")[0]:CANTEEN_ACTIVE_SHEET['J' + str(row)].value.split(":")[1],
            CANTEEN_ACTIVE_SHEET['K' + str(row)].value.split(":")[0]:CANTEEN_ACTIVE_SHEET['K' + str(row)].value.split(":")[1],
            CANTEEN_ACTIVE_SHEET['L' + str(row)].value.split(":")[0]:CANTEEN_ACTIVE_SHEET['L' + str(row)].value.split(":")[1]
        }
    
    return returnObj
